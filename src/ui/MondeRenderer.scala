package ui

import java.awt.Color;
import java.awt.Graphics2D;
import core.Monde;

object MondeRenderer {

  val CELLULE_W = 16

  val CELLULE_H = 16

  private val CELLULE_COLOR = new Color(0, 0, 255)

  private val CONTOUR_COLOR = new Color(200, 200, 200)

  private val FONDS_COLOR = new Color(255, 255, 255)
}

class MondeRenderer(var monde: Monde, private var _view_width: Int, private var _view_height: Int)
    {

      def view_w = _view_width
      def view_h = _view_height
      var view_x = 0
      var view_y = 0

      def view_w_=(width : Int) {
        _view_width = width
        view_x  = width / 2
      }

      def view_h_=(height : Int){
        _view_height = height
        view_y  = height / 2
      }

      private var _largeurTotale  = 0
      private var _hauteurTotale = 0

      def largeurTotale  = _largeurTotale
      def hauteurTotale = _hauteurTotale

      private var cellule_w : Int = MondeRenderer.CELLULE_W
      private var cellule_h: Int = MondeRenderer.CELLULE_H

      private var _zoom = 1.0
      def zoom = _zoom
      def zoom_=(zoom: Double) {
        _zoom = zoom
        cellule_w  = (MondeRenderer.CELLULE_W * zoom).toInt
        cellule_h = (MondeRenderer.CELLULE_H * zoom).toInt
        _largeurTotale  = cellule_w  * monde.largeur
        _hauteurTotale = cellule_h * monde.hauteur
      }

  def setCellule(enVie: Boolean, x: Int, y: Int) {
    val xd = ((view_x - view_w / 2.0) / cellule_w).toInt
    val yd = ((view_y - view_h / 2.0) / cellule_h).toInt
    val cdx = xd * cellule_w - (view_x - view_w / 2)
    val cdy = yd * cellule_h - (view_y - view_h / 2)
    val cx = (x - cdx) / cellule_w
    val cy = (y - cdy) / cellule_h
    monde.setCellule(cx + xd, cy + yd, enVie)
  }


  def render(where: Graphics2D) {
    where.setClip(0, 0, view_w, view_h)
    where.setColor(MondeRenderer.FONDS_COLOR)
    where.fillRect(0, 0, view_w, view_h)

    var xd = ((view_x - view_w / 2.0) / cellule_w).toInt
    var yd = ((view_y - view_h / 2.0) / cellule_h).toInt
    if (xd < 0) xd = 0
    if (yd < 0) yd = 0

    var xf = xd + (view_w / cellule_w)
    var yf = yd + (view_h / cellule_h)
    if (xf >= monde.largeur) xf = monde.largeur - 1
    if (yf >= monde.hauteur) yf = monde.hauteur - 1

    var cx = xd * cellule_w - (view_x - view_w / 2)
    var cy = 0
    val cdy = yd * cellule_h - (view_y - view_h / 2)

    for (x <- xd to xf) {
      cy = cdy
      for (y <- yd to yf) {
        val c = monde.cellules(x)(y)

        where.setColor(MondeRenderer.CONTOUR_COLOR)
        where.drawRect(cx, cy, cellule_w, cellule_h)

        if (c.enVie) {
          where.setColor(MondeRenderer.CELLULE_COLOR)
          where.fillRect(cx, cy, cellule_w, cellule_h)
        }
        cy += cellule_h
      }
      cx += cellule_w
    }
  }
}
