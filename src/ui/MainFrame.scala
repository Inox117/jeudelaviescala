package ui

import java.awt.BorderLayout
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.awt.event.AdjustmentEvent
import java.awt.event.AdjustmentListener
import java.awt.event.ComponentEvent
import java.awt.event.ComponentListener
import javax.swing.BoxLayout
import javax.swing.JButton
import javax.swing.JFrame
import javax.swing.JPanel
import javax.swing.JScrollBar
import javax.swing.JSlider
import javax.swing.event.ChangeEvent
import javax.swing.event.ChangeListener
import core.MainPrg

object MainFrame {
  val FRAME_W = 800
  val FRAME_H = 600
  val TITLE = "Le jeu de la vie - Tp Noté Scala Quentin Victoor"
}

class MainFrame extends JFrame {

  val mondeComponent: MondeComponent = new MondeComponent()
  val horizontalScrollbar: JScrollBar = new JScrollBar()
  val verticalScrollbar: JScrollBar = new JScrollBar()

  setVisible(true)

  setSize(MainFrame.FRAME_W, MainFrame.FRAME_H)
  setTitle(MainFrame.TITLE)

  this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)

  MainPrg.createInstance()

  val panel = new JPanel()
  panel.setLayout(new BorderLayout())
  panel.setSize(MainFrame.FRAME_W, MainFrame.FRAME_H)

  val sp = new JPanel()
  sp.setLayout(new BorderLayout())

  horizontalScrollbar.setOrientation(0)
  horizontalScrollbar.addAdjustmentListener((e: AdjustmentEvent) => {
    mondeComponent.mondeRenderer.view_x = e.getValue + mondeComponent.mondeRenderer.view_w / 2
  })
  horizontalScrollbar.addComponentListener((e: ComponentEvent) => {
    val sc = e.getSource.asInstanceOf[JScrollBar]
    sc.setMaximum(mondeComponent.mondeRenderer.largeurTotale)
    sc.setMinimum(0)
    sc.setVisibleAmount(mondeComponent.mondeRenderer.view_w)
  })

  verticalScrollbar.setOrientation(1)
  verticalScrollbar.addAdjustmentListener((e: AdjustmentEvent) => {
    mondeComponent.mondeRenderer.view_y = e.getValue + mondeComponent.mondeRenderer.view_h / 2
  })
  verticalScrollbar.addComponentListener((e: ComponentEvent) => {
    val sc = e.getSource.asInstanceOf[JScrollBar]
    sc.setMaximum(mondeComponent.mondeRenderer.hauteurTotale)
    sc.setMinimum(0)
    sc.setVisibleAmount(mondeComponent.mondeRenderer.view_h)
  })

  sp.add(mondeComponent, BorderLayout.CENTER)
  sp.add(horizontalScrollbar, BorderLayout.SOUTH)
  sp.add(verticalScrollbar, BorderLayout.EAST)

  val zoom = new JSlider()
  zoom.setOrientation(1)
  zoom.setMaximum(5000)
  zoom.setMinimum(500)
  zoom.setValue(1000)
  zoom.addChangeListener((changeEvent:ChangeEvent) => {
      mondeComponent.mondeRenderer.zoom = changeEvent.getSource.asInstanceOf[JSlider].getValue / 1000.0
      verticalScrollbar.setMaximum(mondeComponent.mondeRenderer.hauteurTotale)
      verticalScrollbar.setMinimum(0)
      verticalScrollbar.setVisibleAmount(mondeComponent.mondeRenderer.view_h)
      horizontalScrollbar.setMaximum(mondeComponent.mondeRenderer.largeurTotale)
      horizontalScrollbar.setMinimum(0)
      horizontalScrollbar.setVisibleAmount(mondeComponent.mondeRenderer.view_w)
  })

  panel.add(sp, BorderLayout.CENTER)
  panel.add(zoom, BorderLayout.EAST)

  val controles = new JPanel()
  controles.setLayout(new BoxLayout(controles, BoxLayout.X_AXIS))

  val start = new JButton("Démarrer")
  start.addActionListener((actionEvent:ActionEvent) => if(MainPrg.instance.paused)MainPrg.instance.reprends())

  val pause = new JButton("Pause")
  pause.addActionListener((actionEvent:ActionEvent) => if(!MainPrg.instance.paused) MainPrg.instance.pause())

  val effacer = new JButton("Effacer")
  effacer.addActionListener((actionEvent:ActionEvent) => MainPrg.instance.monde.tue())

  controles.add(start)
  controles.add(pause)
  controles.add(effacer)
  panel.add(controles, BorderLayout.SOUTH)

  this.setContentPane(panel)

  MainPrg.instance.addObserver(mondeComponent)

  repaint()

//  invalidate()

  implicit def adjustmentListener(function: AdjustmentEvent => Unit): AdjustmentListener =
    new AdjustmentListener() {
      def adjustmentValueChanged(adjustmentEvent: AdjustmentEvent) {
        function(adjustmentEvent)
      }
    }

  implicit def componentListener(function: ComponentEvent => Unit): ComponentListener =
    new ComponentListener() {
      def componentHidden(componentEvent: ComponentEvent) { }
      def componentMoved (componentEvent: ComponentEvent) { }
      def componentShown (componentEvent: ComponentEvent) { }
      def componentResized(componentEvent: ComponentEvent) {
        function(componentEvent)
      }
    }

  implicit def changeListener(function: ChangeEvent => Unit): ChangeListener =
    new ChangeListener() {
      def stateChanged(changeEvent: ChangeEvent){
        function(changeEvent)
      }
    }

  implicit def actionListener(function: ActionEvent => Unit): ActionListener =
    new ActionListener() {
      def actionPerformed(actionEvent: ActionEvent): Unit ={
        function(actionEvent)
      }
    }

  def addControleToPane() = {
    val pane = getContentPane
    val controles = new JPanel()
    controles.setLayout(new BoxLayout(controles, BoxLayout.X_AXIS))
    controles.add(start)
    controles.add(pause)
    controles.add(effacer)
    pane.add(controles, BorderLayout.SOUTH)
  }

}