package ui

import java.awt.Graphics
import java.awt.Graphics2D
import java.awt.event.ComponentEvent
import java.awt.event.ComponentListener
import java.awt.event.MouseEvent
import java.awt.event.MouseListener
import java.awt.event.MouseMotionListener
import java.util.Observable
import java.util.Observer
import javax.swing.JComponent;
import core.MainPrg

class MondeComponent extends JComponent with Observer with MouseListener with MouseMotionListener with ComponentListener {

  var mondeRenderer: MondeRenderer = _

  private var bouton: Int = 0

  this.addMouseListener(this)

  this.addMouseMotionListener(this)

  this.addComponentListener(this)

  repaint()

  override def paintComponent(g: Graphics) {
    if (mondeRenderer == null) {
      mondeRenderer = new MondeRenderer(MainPrg.instance.monde, this.getVisibleRect.width, this.getVisibleRect.height)
      mondeRenderer.view_w_=(mondeRenderer.view_w / 2)
      mondeRenderer.view_h_=(mondeRenderer.view_h / 2)
      setSize(mondeRenderer.view_w, mondeRenderer.view_h)
    }
    mondeRenderer.render(g.asInstanceOf[Graphics2D])
  }

  def update(arg0: Observable, arg1: AnyRef) {
    repaint()
  }

  def mouseClicked(e: MouseEvent) {
    if (e.getButton == MouseEvent.BUTTON1) {
      mondeRenderer.setCellule(true, e.getX, e.getY)
    }
    if (e.getButton == MouseEvent.BUTTON3) {
      mondeRenderer.setCellule(false, e.getX, e.getY)
    }
  }

  def mouseEntered(e: MouseEvent) {
  }

  def mouseExited(e: MouseEvent) {
  }

  def mousePressed(e: MouseEvent) {
    bouton = e.getButton
  }

  def mouseReleased(e: MouseEvent) {
  }

  def mouseDragged(e: MouseEvent) {
    if (bouton == MouseEvent.BUTTON1) {
      mondeRenderer.setCellule(true, e.getX, e.getY)
    }
    if (bouton == MouseEvent.BUTTON3) {
      mondeRenderer.setCellule(false, e.getX, e.getY)
    }
  }

  def mouseMoved(arg0: MouseEvent) {
  }

  def componentHidden(arg0: ComponentEvent) {
  }

  def componentMoved(arg0: ComponentEvent) {
  }

  def componentResized(arg0: ComponentEvent) {
    if (mondeRenderer != null) {
      mondeRenderer.view_w_=(this.getWidth)
      mondeRenderer.view_h_=(this.getHeight)
    }
  }

  def componentShown(arg0: ComponentEvent) {
  }
}