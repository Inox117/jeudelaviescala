package ui

import core.MainPrg

object Launcher {

  def main(args: Array[String]) {
    MainPrg.createInstance()
    new MainFrame()
    MainPrg.instance.start()
    MainPrg.instance.pause()
  }
}