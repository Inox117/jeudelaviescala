package core

/**
 * Classe représentant le Monde du jeu de la vie.
 */

class Monde(var largeur: Int, var hauteur: Int) {
  var cellules: Vector[Vector[Cellule]] = buildVector(new Cellule(false))
//  var voisines: Vector[Vector[Int]] = buildVector(0)

  def getCellule(x: Int, y: Int): Option[Cellule] = {
    if (x >= 0 && y >= 0 &&
        x < largeur && y < hauteur)
      Some(cellules(x)(y))
    else
      None
  }

  def setCellule(x: Int, y: Int, enVie: Boolean) {
    val c = cellules(x)(y)
    if (c.enVie == enVie) return
    c.enVie = enVie
  }

  /**
   * tue toutes les cellules
   */
  def tue() =
    synchronized {
      cellules = Vector.fill(largeur, hauteur)(new Cellule(false))
    }

  private def buildVector[T](func: => T): Vector[Vector[T]] =
    buildVector((_, _) => func)

  private def buildVector[T](func: (Int, Int) => T): Vector[Vector[T]] =
    (for (i <- 0 to largeur)
    yield (for (j <- 0 to hauteur)
      yield func(i, j)
        ).toVector
      ).toVector

  /***
    * mise à jour des cellules
    */
  def update() =
  //Lois pour une cellule :
  //  -Une cellule vivante meure si elle a moins de 2 cellules ou plus de 4 cellules voisines vivantes
  //  -Une cellule morte reviens à la vie si elle a 3 cellules voisines vivantes
    synchronized {
        cellules=buildVector((x,y)=>
          nbVoisinesVivantes(x,y) match{
            case 3 => new Cellule(true)
            case 2 => new Cellule(cellules(x)(y).enVie)
            case _ => new Cellule(false)
          })
//      voisines= buildVector((x,y)=>nbVoisinesVivantes(x,y))
//      for (x <- 0 until largeur){
//        for (y <- 0 until hauteur){
//          voisines(x)(y) match{
//            case 3 => setCellule(x, y, true)
//            case 2 => setCellule(x, y, getCellule(x,y).enVie)
//            case _ => setCellule(x, y, false)
//          }
//        }
//      }
    }

  /***
    * calcule le nombre de cellules vivantes autour de la cellule
    *
    * On récupère les cellules, on filtre les null (sinon les bords posent problèmes) et on compte celle en vie
    *
    * @param x abscisse
    * @param y ordonnée
    * @return nb de cellules voisines vivantes
    */
  private def nbVoisinesVivantes(x: Int, y: Int): Int =
   Vector(
      getCellule(x+1, y+1),
      getCellule(x+1, y),
      getCellule(x+1, y-1),
      getCellule(x, y+1),
      getCellule(x, y-1),
      getCellule(x-1, y+1),
      getCellule(x-1, y),
      getCellule(x-1, y-1)).flatten.count(_.enVie)
}
