package core

import java.util.Observable
import java.util.Timer
import java.util.TimerTask

/**
 * Programme principal du jeu de la vie
 */

object MainPrg {

  val NANO_SECONDES = 1000000000

  val MONDE_W = 800

  val MONDE_H = 600

  val FPS = 25

  var instance: MainPrg = _

  def createInstance(): MainPrg = {
    if (instance == null) instance = new MainPrg()
    instance
  }
}

class MainPrg private () extends Observable {

  private var step_by_step: Boolean = false

  private var refresh_by_sec: Int = 0

  private var derniere_sec: Long = 0

  val monde: Monde = new Monde(MainPrg.MONDE_W, MainPrg.MONDE_H)

  private var started: Boolean = false

  var paused: Boolean = false

  private var startTime: Long = 0

  private var pauseTime: Long = 0

  def start() {
    if (started) throw new RuntimeException("Le monde est déjà démarré")
    started = true
    val period = 1000 / MainPrg.FPS
    val timer = new Timer()
    startTime = System.nanoTime()
    derniere_sec = System.currentTimeMillis()
    timer.scheduleAtFixedRate(new TimerTask() {

      def run() {
        if (!paused) {
          refresh_by_sec += 1
          nextFrame(System.nanoTime() - startTime)
          if (step_by_step) pause()
        }
        if (System.currentTimeMillis() - derniere_sec >= 1000) {
          refresh_by_sec = 0
          derniere_sec = System.currentTimeMillis()
        }
        setChanged()
        notifyObservers()
      }
    }, 0, period)
  }

  def pause() {
    synchronized {
      if (!started) throw new RuntimeException("Le monde n'est pas démarré")
      if (paused) throw new RuntimeException("Le monde est deja en pause !")
      pauseTime = System.nanoTime()
      paused = true
    }
  }

  def reprends() {
    synchronized {
      if (!started) throw new RuntimeException("Le monde n'est pas démarré")
      if (!paused) throw new RuntimeException("Le monde n'est pas en pause !")
      val ecart = System.nanoTime() - pauseTime
      startTime = startTime + ecart
      paused = false
    }
  }

  private def nextFrame(time: Long) {
    synchronized {
      monde.update()
    }
  }
}